package ru.ulstu.is.sbapp;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.ulstu.is.sbapp.operation.service.OperationService;

@SpringBootTest
class SbappApplicationTests {
    @Autowired
    OperationService operationService;

    @Test
    void testSpeakerPow() {
        final Double res = operationService.say("pow", 10, 2);
        Assertions.assertEquals(100, res);
    }

    @Test
    void testSpeakerMin() {
        final Double res = operationService.say("min", 500, 4);
        Assertions.assertEquals(4, res);
    }

    @Test
    void testSpeakerHypot() {
        final Double res = operationService.say("hypot", 3, 4);
        Assertions.assertEquals(5, res);
    }

    @Test
    void testSpeakerErrorWired() {
        Assertions.assertThrows(NoSuchBeanDefinitionException.class, () -> operationService.say("2", 3, 5));
    }
}
