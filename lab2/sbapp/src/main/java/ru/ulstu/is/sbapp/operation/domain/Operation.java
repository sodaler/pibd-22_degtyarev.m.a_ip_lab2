package ru.ulstu.is.sbapp.operation.domain;

public interface Operation {

    Double say(String action, double num1, double num2);
}
