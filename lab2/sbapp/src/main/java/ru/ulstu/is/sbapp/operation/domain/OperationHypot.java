package ru.ulstu.is.sbapp.operation.domain;


public class OperationHypot implements Operation {
    @Override
    public Double say(String action, double num1, double num2){
        return Math.hypot(num1, num2);
    }
}
