package ru.ulstu.is.sbapp.operation.domain;


public class OperationMin implements Operation {
    @Override
    public Double say(String action, double num1, double num2){
        return Math.min(num1, num2);
    }
}
