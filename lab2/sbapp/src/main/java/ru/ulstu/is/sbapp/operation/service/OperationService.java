package ru.ulstu.is.sbapp.operation.service;

import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.ulstu.is.sbapp.operation.domain.Operation;

@Service
public class OperationService {
    private final ApplicationContext applicationContext;

    public OperationService(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

//    public static Object getBean(String action) {
//    }

    public Double say(String action, double num1, double num2) {
        final Operation operation = (Operation) applicationContext.getBean(action);
        return operation.say(action, num1, num2);
    }
}
