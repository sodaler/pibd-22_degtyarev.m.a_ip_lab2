package ru.ulstu.is.sbapp.operation.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.ulstu.is.sbapp.operation.domain.*;

@Configuration
public class OperationConfiguration {

    @Bean(value = "min")
    public OperationMin createSpeakerMin() {
        return new OperationMin();
    }

    @Bean(value = "hypot")
    public OperationHypot createSpeakerHypot() {
        return new OperationHypot();
    }
}
