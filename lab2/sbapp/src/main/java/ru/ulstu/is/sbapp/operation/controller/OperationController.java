package ru.ulstu.is.sbapp.operation.controller;

import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.ulstu.is.sbapp.operation.service.OperationService;

@RestController
public class OperationController {
    private final OperationService operationService;

    public OperationController(ApplicationContext applicationContext) {
        this.operationService = applicationContext.getBean(OperationService.class);
     }

    @GetMapping("/")
    public Double hello(@RequestParam(value = "action", defaultValue = "pow") String action,
                        @RequestParam(value = "num1", defaultValue = "3")  Double num1,
                        @RequestParam(value = "num2", defaultValue = "4")  Double num2) {
        return operationService.say(action, num1, num2);

    }
}
