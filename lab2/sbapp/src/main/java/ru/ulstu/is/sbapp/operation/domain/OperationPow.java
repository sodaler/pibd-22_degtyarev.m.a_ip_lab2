package ru.ulstu.is.sbapp.operation.domain;

import org.springframework.stereotype.Component;

@Component(value = "pow")
public class OperationPow implements Operation {
    @Override
    public Double say(String action, double num1, double num2){
        return Math.pow(num1, num2);
    }
}
